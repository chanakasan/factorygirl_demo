require "rails_helper"

RSpec.describe Studio, :type => :model do
  it 'has a name' do
    expect(subject.name).to be_nil

    subject.name = 'Test Studio'
    expect(subject.name).to eql('Test Studio')
  end

  it 'has a subdomain' do
    expect(subject.subdomain).to be_nil

    subject.subdomain = 'teststudio'
    expect(subject.subdomain).to eql('teststudio')
  end

  it 'has a primary teacher' do
    expect(subject.primary_teacher).to be_nil

    new_teacher = FactoryGirl.build(:user)
    subject.primary_teacher = new_teacher
    expect(subject.primary_teacher).to eql(new_teacher)
  end
end
