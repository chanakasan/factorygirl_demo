require "rails_helper"

RSpec.describe User, :type => :model do
  it 'has a first name' do
    expect(subject.first_name).to be_nil

    subject.first_name = 'John'
    expect(subject.first_name).to eql('John')
  end

  it 'has a last name' do
    expect(subject.last_name).to be_nil

    subject.last_name = 'Doe'
    expect(subject.last_name).to eql('Doe')
  end
end
