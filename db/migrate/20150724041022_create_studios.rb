class CreateStudios < ActiveRecord::Migration
  def change
    create_table :studios do |t|
      t.string :name
      t.string :subdomain, limit: 20
      t.references :primary_teacher, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :studios, :subdomain, unique: true
  end
end
