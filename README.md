### Using FactoryGirl in a rails app

**Try it out!**

```
git clone https://chanakasan@bitbucket.org/chanakasan/factorygirl_demo.git factorygirl_demo
cd factorygirl_demo
bundle install
rspec
```

**Resources**
* [FactoryGirl docs](http://www.rubydoc.info/gems/factory_girl/file/GETTING_STARTED.md)
* [Rspec model specs](https://www.relishapp.com/rspec/rspec-rails/v/3-3/docs/model-specs)
